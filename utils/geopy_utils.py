from geopy.geocoders import Nominatim
from geopy.geocoders import GoogleV3

class Geo:
    @staticmethod
    def get_address_from_coordinates(latitude: float, longitude: float) -> dict:
        """
            This generates address based on GPS coordinates which are usually expressed as 
            the combination of latitude and longitude.

            :param latitude: latitude
            :type float: longitude of type float

            :param longitude: longitude.
            :type float: longitude of type float.

            :return: Returns combination of latitude,longitude':longitude,road,city_district,
                     city,county,state,country,postcode
            :rtype: dict.

            :raises TypeError: Raise TypeError(None) if there is no location posible for given 
                               latitude or longitude, then return None
            """        
        geolocator = Nominatim(user_agent="address_generator")

        # Combine latitude and longitude into a single string
        coordinates = f"{latitude}, {longitude}"

        try:
          # Use reverse geocoding to get the address
          location = geolocator.reverse(coordinates)
          if not location:
              raise TypeError(None)
          
          location = location.raw
      
          # Extract the granular details of address from the location object
          address = {
              'latitude':latitude,
              'longitude':longitude,
              'road':location.get('address').get('road'),
              'city_district':location.get('address').get('city_district'),
              'city':location.get('address').get('city'),
              'county':location.get('address').get('county'),
              'state':location.get('address').get('state'),
              'country':location.get('address').get('country'),
              'postcode':location.get('address').get('postcode')
          }
          return address
        except:
            return None