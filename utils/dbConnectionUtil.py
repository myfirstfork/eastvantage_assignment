from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import os
from config.settings import DB_NAME

class dbConnectionUtil():
    BASE_DIR = os.getcwd()

    @staticmethod
    def get_database_engine():
        try:
            url = "sqlite:///"+os.path.join(dbConnectionUtil.BASE_DIR,DB_NAME)
            engine = create_engine(url,echo=False)
        except IOError as ex:
            raise ex
        return engine

    @staticmethod
    def get_database_session():
        engine = dbConnectionUtil.get_database_engine()
        Session = sessionmaker(bind=engine, autocommit=False)
        
        session = Session()
        return session