import uuid
from sqlalchemy import Column,String, Integer, DateTime,VARCHAR, Float, Text, UniqueConstraint
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import func
from config.settings import ADDRESS_BOOK
Base = declarative_base()

class AddressBook(Base):
    __tablename__ = ADDRESS_BOOK
    id = Column("address_id",String(255),primary_key=True,nullable=False)
    latitude = Column("latitude",Float,unique=False, nullable=False)
    longitude = Column("longitude",Float,unique=False, nullable=False)
    road = Column('road',String,unique=False, nullable=True)
    city_district = Column('city_district',String,unique=False, nullable=True)
    city = Column('city',String,unique=False, nullable=True)
    county = Column('county',String,unique=False, nullable=True)
    state = Column('state',String,unique=False, nullable=True)
    country = Column('country',String,unique=False, nullable=True)
    postcode = Column('postcode',String,unique=False, nullable=True)
    
    created_date = Column("CreatedDate",DateTime, nullable=False, default=func.now())
    updated_date = Column("UpdatedDate",DateTime, onupdate=func.now())

    UniqueConstraint(latitude, longitude, name='uq_combination')
           
            
            