from sqlalchemy.orm.session import Session
from sqlalchemy.orm import load_only
from utils.dbConnectionUtil import dbConnectionUtil
from sqlalchemy import exc
from exceptions.db import DbException
from sqlalchemy import exc, and_

class ServiceBase():
    def __init__(self,model):
        self.model = model
        self.session: Session = dbConnectionUtil.get_database_session()

    def find_one(self, findOneOptions):
        """
        This is a generic function find first occurrence of a row based on a given condition.

        :param findOneOptions: condition to filter data.
        :type findOneOptions: text.

        :return: Return result of SQLALchemy query.
        :rtype: SQLAlchemy object

        :raises Exception: If query fails in DB

        """
        try:
            # get all the columns in the table
            results = self.session.query(self.model).filter(findOneOptions).first()
            return results
        except exc.SQLAlchemyError as e:
            self.session.rollback()
            raise DbException(e)
        except Exception as e:
            raise e
        finally:
            self.session.close()

    def create_one(self,_tuple):
        """
        This is a generic function to create new row.

        :param _tuple: condition to filter data.
        :type _tuple: AddressBook type object

        :return: None on success.
        :rtype: None

        :raises Exception: If query fails in DB

        """        
        try:
            self.session.add(_tuple)
            self.session.flush()
            self.session.commit()
        except Exception as E:
            self.session.rollback()
            raise E
        finally:
            self.session.close()

    def update_one(self,update_these,uid):
        """
        This is a generic function to update row on given condition.

        :param update_these: fields to be updated packed in a dictionary.
        :type update_these: dict.

        :param uid: uuid.
        :type uid: str.

        :return: None on success.
        :rtype: None

        :raises Exception: If query fails in DB

        """        
        #update using SQLAlchemy
        try:
            self.session.query(self.model).\
            filter(and_(self.model.id == uid)).\
            update(update_these)
            self.session.commit()
        except exc.SQLAlchemyError as e:
            self.session.rollback()
            raise DbException(e)
        except Exception as e:
            raise e
        finally:
            self.session.close()

    def delete_one(self, uid):
        """
        This is a generic function to delete a row based on a given condition.

        :param uid: uuid.
        :type uid: str.

        :return: None on success.
        :rtype: None

        :raises Exception: If query fails in DB

        """           
        try:
            results = self.session.query(self.model).get(uid)
            self.session.delete(results)
            self.session.commit()
        except exc.SQLAlchemyError as e:
            self.session.rollback()
            raise DbException(e)
        except Exception as e:
            raise e
        finally:
            self.session.close()