# PYTHON REST APIS - Eastvantage Assignment

## ENVIRONMENT SETUP

### Prerequisites
Please make sure you have,
- It is expected that you are setting up environment on windows system and using VS code as IDE.
- Install Python 3.10 on Windows
- CMD is preferred to act as a terminal for below procedure.

### CREATE A VIRTUAL ENVIRONMENT
Once above prerequisites are met, you can use built-in venv module to setup virtual enviroment for Eastvantage Assignment
To create a virtual environment, open your terminal (command prompt) and navigate to the directory where you want to create the virtual environment (preferably your current working directory). Then, run the following command:
NOTE: Replace <venv_name>-->replace this with the name you want to give to your virtual environment.
#### `python -m venv <venv_name>`

### ACTIVATE THE VIRTUAL ENVIRONMENT
On Windows:
#### `<venv_name>\Scripts\activate`

If virtual environment is not started, please close the terminal and open again then try.
Even if it is not started please, follow: 
- Press `Ctrl + Shift + P` and search for `Python: Interpreter`
- In the next selection, choose the python project.
- In the next selection, choose the virtual environment you have created.
- Add a new terminal, it should auto activate the new virtual environment

### ON ACTIVATION OF THE VIRTUAL ENVIRONMENT
Once the virtual environment is activated, you'll notice that your terminal prompt changes to show the name of the virtual environment (e.g., <venv_name>).

### INSTALL DEPEDENCIES
In order to setup development or testing environment, please execute below command in your terminal (CMD)
`pip install -r requirements.txt`

### INITIALIZE ENVIRONMENT VARIABLE
In order to initialize environment variables, please execute below command in your terminal (CMD)
`command.bat`

#### SETUP DONE!!!

#### EXECUTE AND TEST THE APPLICATION

### EXECUTION:
In order to execute Eastvantage Assignment, please execute below command in your terminal (CMD) and Make sure no other service is running on 9000
`python -m uvicorn services.address_service.address_svc:app --port 9000`

### TESTING APPLICATION
In order to test Eastvantage Assignment, please open below link in your browser
`http://127.0.0.1:9000/docs or http://localhost:9000/docs`
