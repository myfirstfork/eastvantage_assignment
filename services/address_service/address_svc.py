from fastapi.responses import JSONResponse
from common.fastapi_app import app
from fastapi import Path, HTTPException
from sqlalchemy.exc import IntegrityError
from services.address_service.schema.address_svc_schema import AddressSchema, AddressUpdateByIDSchema
from utils.geopy_utils import Geo
from services.address_service.svc.address_book_table import AddressBookTable
from common.models import Base, AddressBook
from utils.dbConnectionUtil import dbConnectionUtil
import uuid

engine = dbConnectionUtil()
Base.metadata.create_all(engine.get_database_engine(),tables=[AddressBook.__table__], checkfirst=True)

address_table = AddressBookTable()

@app.post("/v1/create-address")
def create_address(req_json: AddressSchema):
    address = Geo().get_address_from_coordinates(req_json.latitude, req_json.longitude)
    try:
        if address:
            address_table.insert_row(req_json.id,address)

            #preparation of response
            response = ""
            for key,val in address.items():
                if key=='latitude' or key==key=='longitude':
                    continue
                elif val:
                    response+=val+','

            return JSONResponse(content=f"ID - {req_json.id} , Address : {response} at {req_json.latitude}°N & {req_json.longitude}°E", 
                                status_code=201)
        else:
            return JSONResponse(content=f"Geopy in Python is not capable to obtain location at {req_json.latitude}°N & {req_json.longitude}°E", 
                                status_code=422)# Unprocessable Entity

    except IntegrityError:
        raise HTTPException(status_code=400, detail="Address with these coordinates already exists")

@app.put("/v1/update-address/{id}")
def update_item(id : uuid.UUID = Path(..., title="The ID of the address to update"), req_json: AddressUpdateByIDSchema=None):
    if req_json:
        #check id is valid or not:
        result = address_table.find_id(str(id))
        
        if result:

            #check if both latitude and longitude are duplicate:
            if req_json.latitude == result.latitude and req_json.longitude == result.longitude:
                return JSONResponse(content=f"{req_json.latitude} & {req_json.longitude} both are present in DB for id-{id}", status_code=404)
            
            #get latitude
            if not req_json.latitude:
                req_json.latitude = result.latitude
            
            #get longitude
            if not req_json.longitude:
                req_json.longitude = result.longitude

            #get updated address
            address = Geo().get_address_from_coordinates(req_json.latitude, req_json.longitude)

            if address:
                address_table.update_row(str(id),address)

                response = ""
                for key,val in address.items():
                    if key=='latitude' or key==key=='longitude':
                        continue
                    elif val:
                        response+=val+','

                return JSONResponse(content=f"Address Updated as : {response} at {req_json.latitude}°N & {req_json.longitude}°E", 
                                        status_code=200)
            else:
                return JSONResponse(content=f"Geopy in Python is not capable to obtain location at {req_json.latitude}°N & {req_json.longitude}°E", 
                                        status_code=422)# Unprocessable Entity

        else:
            return JSONResponse(content=f"id-{id} is not present in DB", status_code=400)


@app.delete("/v1/delete-address/{id}")
def delete_address(id : uuid.UUID = Path(..., title="The ID of the address to update")):
    result = address_table.find_id(str(id))
    if result:
        result = address_table.delete_address(str(id))
        return JSONResponse(content=f"u{id} deleted", status_code=200)
    else:
        return JSONResponse(content=f"id-{id} is not present in DB", status_code=400)

    
