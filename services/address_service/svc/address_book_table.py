from common.ServiceBase import ServiceBase
from common.models import AddressBook
from sqlalchemy import text

class AddressBookTable(ServiceBase):
    def __init__(self):
        super().__init__(AddressBook)
    
    def insert_row(self,id,address):
        _tuple = AddressBook(id=str(id),
                          latitude=address.get('latitude'),
                          longitude=address.get('longitude'),
                          road = address.get('road'),
                          city_district = address.get('city_district'),
                          city=address.get('city'),
                          county=address.get('county'),
                          state=address.get('state'),
                          country=address.get('country'),
                          postcode=address.get('postcode')
        )

        self.create_one(_tuple)

    def find_id(self,id):
        try:
            condition = text("address_id={}".format(repr(id)))
            return self.find_one(condition)
        except Exception as e:
            print("Error - ",e)

    def update_row(self, id, address):     
        update_these={'latitude': address.get('latitude'), 
         'longitude': address.get('longitude'), 
         'road': address.get('road'), 
         'city_district': address.get('city_district'), 
         'city': address.get('city'), 
         'county': address.get('county'), 
         'state': address.get('state'), 
         'country': address.get('country'), 
         'postcode': address.get('postcode')
         }
        
        self.update_one(update_these,id)

    def delete_address(self,id):
        self.delete_one(id)