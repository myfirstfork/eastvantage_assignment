from pydantic import BaseModel, Field, validator
from typing import Optional
import uuid
from config.settings import LATITUDE_MIN, LATITUDE_MAX, LONGITUDE_MIN, LONGITUDE_MAX

class AddressSchema(BaseModel):
    id : uuid.UUID = Field(default_factory=uuid.uuid4)
    latitude: float = Field(...,ge=LATITUDE_MIN,le=LATITUDE_MAX, alias="Latitude",description="A float with a maximum of 10 digits after the decimal point")
    longitude: float = Field(...,ge=LONGITUDE_MIN,le=LONGITUDE_MAX, alias="Longitude",description="A float with a maximum of 10 digits after the decimal point")

    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    'Latitude': 12.9319,
                    'Longitude': 77.6065
                }
            ]
        }
    }

    class config:
        allow_population_by_field_name=True

    @validator('latitude')
    def validate_latitude(cls, value):
        if value is not None:
            # Specify the maximum number of decimal places for latitude you want to allow
            max_decimal_places = 10
            str_value = str(value)
            decimal_places = len(str_value.split('.')[-1])
            if decimal_places > max_decimal_places:
                raise ValueError(f"Number of decimal places of should be at most {max_decimal_places}")
        return value 

    @validator('longitude')
    def validate_longitude(cls, value):
        if value is not None:
            # Specify the maximum number of decimal places for longitude you want to allow
            max_decimal_places = 10
            str_value = str(value)
            decimal_places = len(str_value.split('.')[-1])
            if decimal_places > max_decimal_places:
                raise ValueError(f"Number of decimal places should be at most {max_decimal_places}")
        return value  
    

class AddressUpdateByIDSchema(BaseModel):
    latitude: Optional[float] = Field(default=None,ge=LATITUDE_MIN,le=LATITUDE_MAX, alias="updated_latitude",description="A float with a maximum of 10 digits after the decimal point")
    longitude: Optional[float] = Field(default=None,ge=LONGITUDE_MIN,le=LONGITUDE_MAX, alias="updated_longitude",description="A float with a maximum of 10 digits after the decimal point")

    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    'updated_latitude': 12.9319,
                    'updated_longitude': 20.992345
                }
            ]
        }
    }
