class ExceptionBase(Exception):
    def __init__(self,message:str):
        self.type = self.__class__.__name__
        self.message = message

    def get_exception(self):
        return {
            'type': self.type,
            'message': self.message,
        }    