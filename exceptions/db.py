from .base import ExceptionBase
class DbException(ExceptionBase):
    def __init__(self,message):
        super().__init__(message)
    
    def get_exception(self):
        return super().get_exception()    